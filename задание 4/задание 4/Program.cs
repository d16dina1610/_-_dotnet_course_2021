﻿using System;

namespace Task_4
{
    class Program
    {
        static void Main(string[] args)
        {
            int position = 0;

            int value = 0;

            Console.Write("Введите размер массива: ");
            int countArray = int.Parse(Console.ReadLine());
            int[] array = new int[countArray];

            for (int i = 0; i < array.Length - 1; i++)
            {
                Console.Write($"Введите элемент массива[" + (i + 1) + "]: ");
                array[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Вывод массива: ");

            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine("[" + (i + 1) + "]:" + array[i]);
            }

            Console.Write("Введите позицию : ");
            position = Convert.ToInt32(Console.ReadLine());

            Console.Write("Введите новое значение для этой позиции : ");
            value = Convert.ToInt32(Console.ReadLine());

            for (int i = array.Length-1; i >= position; i--)
            {
                array[i] = array[i - 1];
            }

            array[position - 1] = value;

            Console.WriteLine("Массив после добавления нового элемента : ");

            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine("Элемент[" + (i + 1) + "]: " + array[i]);
            }

            Console.WriteLine();
        }
    }
}