﻿using System;

namespace Task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите первое число:");
            int firstValue = Convert.ToInt32(Console.ReadLine());
           
            Console.WriteLine("Введите второе число:");
            int secondValue = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Введите третее число:");
            int thirdValue = Convert.ToInt32(Console.ReadLine());

            if (firstValue < secondValue && secondValue < thirdValue)
            {
                Console.WriteLine($"По возрастанию:{firstValue},{secondValue},{thirdValue}");
            }

            else if (firstValue>secondValue && firstValue>thirdValue && secondValue<thirdValue)
            {
                Console.WriteLine($"По возрастанию:{firstValue},{thirdValue},{secondValue}");
            }

            else if(secondValue>firstValue && firstValue>thirdValue)
            {
                Console.WriteLine($"По возрастанию:{secondValue},{firstValue},{thirdValue}");
            }

            else if (secondValue > thirdValue && firstValue < thirdValue) 
            { 
                Console.WriteLine($"По возрастанию:{secondValue},{thirdValue},{firstValue}"); 
            }

            else if (thirdValue > firstValue && firstValue > secondValue) 
            {
                Console.WriteLine($"По возрастанию:{thirdValue},{firstValue},{secondValue}");
            }

            else if (thirdValue > secondValue && firstValue < secondValue) 
            {
                Console.WriteLine($"По возрастанию:{thirdValue},{secondValue},{firstValue}");
            }

            if (firstValue < secondValue && secondValue < thirdValue)
            {
                Console.WriteLine($"По Убыванию:{thirdValue},{secondValue},{firstValue}");
            }

            else if (firstValue > secondValue && firstValue > thirdValue && secondValue < thirdValue)
            {
                Console.WriteLine($"По Убыванию:{secondValue},{thirdValue},{firstValue}");
            }

            else if (secondValue > firstValue && firstValue > thirdValue)
            {
                Console.WriteLine($"По Убыванию:{thirdValue},{firstValue},{secondValue}");
            }

            else if (secondValue > thirdValue && firstValue < thirdValue)
            {
                Console.WriteLine($"По Убыванию:{firstValue},{thirdValue},{secondValue}");
            }

            else if (thirdValue > firstValue && firstValue > secondValue)
            {
                Console.WriteLine($"По Убыванию::{secondValue},{firstValue},{thirdValue}");
            }

            else if (thirdValue > secondValue && firstValue < secondValue)
            {
                Console.WriteLine($"По Убыванию:{firstValue},{secondValue},{thirdValue}");
            }

            Console.WriteLine($"Максимальное число={Math.Max(Math.Max(firstValue, secondValue), thirdValue) }");

            Console.WriteLine($"Минимально число ={Math.Min(Math.Min(firstValue, secondValue), thirdValue) }");
            
        }
    }
}
