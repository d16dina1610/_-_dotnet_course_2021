﻿using System;

namespace Task_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите число: ");

            string inputValue = Console.ReadLine();
            int outputValue;

            if (int.TryParse(inputValue, out outputValue))
            {
                Console.WriteLine($"Преобразование произошло,ваше число={outputValue}"); ;
            }
            else
            { 
                Console.WriteLine("Введите другое число: ");
            }

            Console.ReadLine();
        }
    }
}


