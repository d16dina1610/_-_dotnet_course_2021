﻿using System;

namespace Task_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите сколько будет элементов в массиве : ");
            int countArray = int.Parse(Console.ReadLine());
            int[] array = new int[countArray];

            for (int i = 0; i < array.Length; i++)
            {
                Console.Write($"Заполните массив элементом с индексом {i}: ");
                array[i] = int.Parse(Console.ReadLine());
            }

            Console.WriteLine("Ваш массив: ");

            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }

            int element = 0;
            int count = 0;

            for(int j = 0; j < array.Length; j++)
            {
                int tempElement = array[j];
                int tempCount = 0;

                for(int p = 0; p < array.Length; p++)
                {

                    if (array[p] == tempElement)
                    {
                        tempCount++;
                    }

                    if (tempCount > count)
                    {
                        element = tempElement;
                        count = tempCount;
                    }
                }
            }

            Console.WriteLine($"Число которе повторяется чаще всего:{element}");
            Console.WriteLine($"Сколько раз:{count}");
        }
    }
}
